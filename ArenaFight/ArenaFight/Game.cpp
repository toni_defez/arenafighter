#include "Game.h"
#include "GameState.h"
#include <array>

Game::Game():m_window(sf::VideoMode(480, 500), "ArenaFighter")
{
	//cargamos la fuente
	if (!m_font.loadFromFile("assets/font.ttf"))
	{
		//lanzando una excepcion  con mensaje personalizado
		throw std::runtime_error("Unable to load the font file");
	}
	//inicializamos el conjuntos de estados
	m_gameStates[GameState::Init ] = new InitState(this);
	m_gameStates[GameState::Ready] = new ReadyState(this);
	m_gameStates[GameState::Option] = new OptionsState(this);
	m_gameStates[GameState::Play] = new PlayingState(this);
	m_gameStates[GameState::Lost] = new LostState(this);
	//el estado incial
	changeGameState(GameState::Init);

	//el juego no esta finalizado
	finished = false;
}

void Game::run()
{
	
	sf::Clock frameclock;
	while (m_window.isOpen())
	{
		sf::Event event;
		//manejador de eventos
		while (m_window.pollEvent(event) )
		{
			if (event.type == sf::Event::Closed || finished)
				m_window.close();

			//controlando  las pulsaciones de teclado
			//con el evento 

			if (event.type == sf::Event::KeyPressed)
			{
				if (event.key.code == sf::Keyboard::I)
					m_currentState->insertCoin();
				if (event.key.code == sf::Keyboard::P)
					m_currentState->pressButton();
				//Moviendo al personaje
				
				if (event.key.code == sf::Keyboard::A)//left
					m_currentState->moveStick(sf::Vector2i(-1, 0));

				if (event.key.code == sf::Keyboard::D)//right
					m_currentState->moveStick(sf::Vector2i(1, 0));

				if (event.key.code == sf::Keyboard::W)//up
					m_currentState->moveStick(sf::Vector2i(0, -1));

				if (event.key.code == sf::Keyboard::S)//down
					m_currentState->moveStick(sf::Vector2i(0, 1));
					
			}
		}
		/*Para cada estado mostrara
		su upadte, su draw y demas cosas
		*/
		m_currentState->update(frameclock.restart());
		m_window.clear();
		m_currentState->draw(m_window);
		m_window.display();

	}

}

void Game::changeGameState(GameState::State gameState)
{
	m_currentState = m_gameStates[gameState];
}

sf::Font& Game::getFont()
{
	return m_font;
}

sf::Vector2f Game::getDimensions()
{
	return sf::Vector2f(m_window.getSize().x, m_window.getSize().y);
}

bool Game::IsFinished()
{
	return finished;
}

void Game::setFinish(bool finish)
{
	finished = finish;
}



Game::~Game()
{
	for (GameState* gameState : m_gameStates)
		delete gameState;
}
