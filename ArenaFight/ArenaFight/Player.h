#ifndef ARENA_FIGHTER_PLAYER_HPP
#define ARENA_FIGHTER_PLAYER_HPP
#include "SFML\Graphics.hpp"
#include "Character.h"
class Player : public Character
{
public:
	Player();
	void draw(sf::RenderTarget& target, sf::RenderStates states)const;
	void update(sf::Time delta);
	void setDirection(sf::Vector2i direction);
	~Player();

private:
	sf::CircleShape m_form;
	bool pressed;
};

#endif // ARENA_FIGHTER_PLAYER_HPP