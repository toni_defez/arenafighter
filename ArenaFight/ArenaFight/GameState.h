#ifndef ARENA_FIGHT_GAMESTATE_HPP
#define ARENA_FIGHT_GAMESTATE_HPP
#define MAX_NUMBER_OF_ITEMS 3

#include <SFML/Graphics.hpp>
#include "Player.h"
class Game;

class GameState
{
public:

	enum State
	{
		Init,
		Ready,
		Option,
		Play,
		Lost,
		Count
	};
	GameState(Game* game);
	Game* getGame() const;

	virtual void insertCoin() = 0;
	virtual void pressButton() = 0;
	virtual void update(sf::Time delta) = 0;
	virtual void moveStick(sf::Vector2i direction);
	virtual void draw(sf::RenderWindow& window) = 0;

private:
	Game* m_game;
};


/*Pantalla de inicio*/
class InitState : public GameState
{
public:
	InitState(Game* game);

	void insertCoin();
	void pressButton();
	void update(sf::Time Delta);
	void moveStick(sf::Vector2i direction);
	void draw(sf::RenderWindow& window);
private:
	sf::Text m_text[MAX_NUMBER_OF_ITEMS];
	sf::Text m_title;
	int selectItem;

	sf::Sprite m_spriteBackground;
	sf::Texture m_textureBackground;

};

/*Pantalla antes juego*/
class ReadyState : public GameState
{
public:
	ReadyState(Game* game);
	void insertCoin();
	void pressButton();
	void update(sf::Time Delta);
	void moveStick(sf::Vector2i direction);
	void draw(sf::RenderWindow& window);
private:
	sf::Text m_text;
};

/*Pantalla de scores/otros*/


class OptionsState : public GameState
{
public:
	OptionsState(Game* game);
	void insertCoin();
	void pressButton();
	void update(sf::Time Delta);
	void moveStick(sf::Vector2i direction);
	void draw(sf::RenderWindow& window);
private:
	sf::Text m_text;
};

class PlayingState : public GameState
{
public:
	PlayingState(Game* game);
	void insertCoin();
	void pressButton();
	void update(sf::Time Delta);
	void moveStick(sf::Vector2i direction);
	void draw(sf::RenderWindow& window);
	void updateCamera();
private:
	sf::Text m_text;
	Player* m_player1;
	sf::View m_camera;

};

class LostState : public GameState
{
public:
	LostState(Game* game);
	void insertCoin();
	void pressButton();
	void update(sf::Time Delta);
	void moveStick(sf::Vector2i direction);
	void draw(sf::RenderWindow& window);
private:
	sf::Text m_text;
};

#endif

