#include "Game.h"
#include "GameState.h"
#include <vector>
#include<iostream>

/*************/
//Esta funcion sirve para poner cualquier elemento
//su punto de origen en el centro

template <typename T>
void centerOrigin(T& drawable)
{
	sf::FloatRect bound = drawable.getLocalBounds();
	drawable.setOrigin(bound.width / 2, bound.height / 2);
}
/***************/

GameState::GameState(Game* game) :m_game(game)
{
}



Game * GameState::getGame() const
{
	return m_game;
}

void GameState::draw(sf::RenderWindow & window)
{
}
void GameState::update(sf::Time delta)
{
}
void GameState::moveStick(sf::Vector2i direction)
{

}
/*__________*/
InitState::InitState(Game * game):GameState(game)
{
	selectItem = 0;
	std::cout << "Estoy en pantalla de inicio" << std::endl;

	//To DO NO SE SI ESTO ESTA BIEN EN ESTE LUGAR
	//no carga bien la textura de fondo 
	/*
	if (m_textureBackground.loadFromFile("assets/a.jpg"))
	{
		throw std::runtime_error("Unable to load the font file");
	}
	*/
	//m_spriteBackground.setTexture(m_textureBackground);
	m_title.setFont(game->getFont());
	m_title.setString("ArenaFighter");
	m_title.setCharacterSize(62);
	m_title.setPosition(30, 30);

	//Creando pantalla de menu
	std::string cadena = "";
	for (int i = 0; i < MAX_NUMBER_OF_ITEMS; i++)
	{
		m_text[i].setFont(game->getFont());

		if(selectItem==i)
			m_text[i].setFillColor(sf::Color::Red);
		else
			m_text[i].setFillColor(sf::Color::White);

		cadena = i == 0 ? "Inicio" : i==1?"Options":"Exit";

		m_text[i].setString(cadena);

		sf::Vector2f dimension = getGame()->getDimensions();
		m_text[i].setPosition(30, dimension.y / (MAX_NUMBER_OF_ITEMS + 1) *( i+1));
	}

}

void InitState::insertCoin()
{
	if (selectItem == 0)
		getGame()->changeGameState(GameState::Ready);
	else if (selectItem == 1) 
	{
		//to do ir a opciones
		getGame()->changeGameState(GameState::Option);
	}
	else
		//to do ir salir del juego
		getGame()->setFinish(true);

}

void InitState::pressButton()
{
}

void InitState::update(sf::Time Delta)
{
	for (int i = 0; i < MAX_NUMBER_OF_ITEMS; i++)
	{
		if (selectItem == i)
		{
			m_text[i].setFillColor(sf::Color::Red);
			m_text[i].setCharacterSize(42);
		}
		else
		{
			m_text[i].setFillColor(sf::Color::White);
			m_text[i].setCharacterSize(32);
		}
	}
}

//en coinState los unicos movimientos que podemos hacer
//es ir arriba a abajo
void InitState::moveStick(sf::Vector2i direction)
{

	if (direction.x == 0)
	{
		if (direction.y == -1)//up
		{
			selectItem--;
			selectItem = selectItem < 0 ? 0 : selectItem;
		}
		else//down
		{
			selectItem++;
			selectItem = selectItem > 2 ? 2 : selectItem;
		}

		std::cout << selectItem << std::endl;
		
		
	}
}

void InitState::draw(sf::RenderWindow& window)
{

	window.draw(m_spriteBackground);

	for (int i = 0; i < MAX_NUMBER_OF_ITEMS; i++)
		window.draw(m_text[i]);

	window.draw(m_title);

}

/*__________*/

ReadyState::ReadyState(Game * game): GameState(game)
{
	std::cout << "Estoy en pantalla de Ready" << std::endl;
	m_text.setFont(game->getFont());
	m_text.setString("Ready?");
	
	m_text.setPosition(240, 150);
}

void ReadyState::insertCoin()
{
	getGame()->changeGameState(GameState::Play);
}

void ReadyState::pressButton()
{
}

void ReadyState::update(sf::Time Delta)
{
}

void ReadyState::moveStick(sf::Vector2i direction)
{
}

void ReadyState::draw(sf::RenderWindow& window)
{
	
	window.draw(m_text);
}

/**********************/
OptionsState::OptionsState(Game* game) : GameState(game)
{
	m_text.setFont(game->getFont());
	m_text.setString("Options press i to go back menu");

	m_text.setPosition(240, 150);
}

void OptionsState::insertCoin()
{
	getGame()->changeGameState(GameState::Init);
}

void OptionsState::pressButton()
{
}

void OptionsState::update(sf::Time Delta)
{
}

void OptionsState::moveStick(sf::Vector2i direction)
{
}

void OptionsState::draw(sf::RenderWindow & window)
{
	window.draw(m_text);
}


/****____***/
PlayingState::PlayingState(Game * game):GameState(game),
m_player1(nullptr)
{
	m_text.setFont(game->getFont());
	m_text.setString("Gaming!!!");
	
	m_player1 = new Player();
	m_text.setPosition(240, 150);
}

void PlayingState::insertCoin()
{
	getGame()->changeGameState(GameState::Lost);
}

void PlayingState::pressButton()
{

}

void PlayingState::update(sf::Time Delta)
{
	m_player1->update(Delta);
	updateCamera();

}

void PlayingState::moveStick(sf::Vector2i direction)
{
	m_player1->setDirection(direction);
	
}

void PlayingState::draw(sf::RenderWindow & window)
{
	window.setView(m_camera);
	window.draw(m_text);
	window.draw(*m_player1);
}

void PlayingState::updateCamera()
{
	m_camera.setCenter(m_player1->getPosition());
	//centrando la imagen cuando se encuentre por el borde 
	if (m_camera.getCenter().x < 240)
		m_camera.setCenter(240, m_camera.getCenter().y);
	if (m_camera.getCenter().y < 240)
		m_camera.setCenter(m_camera.getCenter().x, 240);

	

}


/*LOST!!!*/
LostState::LostState(Game * game) :GameState(game)
{
	m_text.setFont(game->getFont());
	m_text.setString("Lost!!!");

	m_text.setPosition(240, 150);
}

void LostState::insertCoin()
{
}

void LostState::pressButton()
{
}

void LostState::update(sf::Time Delta)
{
}

void LostState::moveStick(sf::Vector2i direction)
{
	getGame()->changeGameState(GameState::Init);
}

void LostState::draw(sf::RenderWindow & window)
{
	window.draw(m_text);
}
