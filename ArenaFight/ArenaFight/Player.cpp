#include "Player.h"



Player::Player()
{
	m_form.setRadius(50.f);
	m_form.setFillColor(sf::Color::Green);
	pressed = false;
}

void Player::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_form, states);
	
}

void Player::update(sf::Time delta)
{
	//if(pressed)
		Character::update(delta);

	pressed = false;
}

void Player::setDirection(sf::Vector2i direction)
{
	Character::setDirection(direction);
	pressed = true;
}


Player::~Player()
{
}
