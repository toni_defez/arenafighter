#pragma once
#ifndef ARENA_FIGHTER_CHARACTER_HPP
#define ARENA_FIGHTER_CHARACTER_HPP
#include<SFML\Graphics.hpp>
#include <array>

class Character:public sf::Drawable, public sf::Transformable
{
public:
	Character();
	~Character();
	virtual void update(sf::Time delta);
	void setDirection(sf::Vector2i direction);
	sf::Vector2i getDirection() const;

	void setSpeed(sf::Vector2f speed);
	sf::Vector2f getSpeed() const;
	sf::Vector2f getAcceleration() const;
	void setAcceleration(sf::Vector2f acceleration) ;
protected:
	virtual void changeDirection() {};
private:
	sf::Vector2i m_currentDirection;
	sf::Vector2i m_nextDirection;
	std::array<bool, 4> m_availableDirections;
	sf::Vector2f m_speed;
	sf::Vector2f m_aceleration;

	//Ya no se si esto esta bien
	/*
	const float maxVelocity = 50;;
	const float minVelocity=0;
	int roz = 0;
	*/
};


#endif 