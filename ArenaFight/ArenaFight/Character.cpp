#include "Character.h"
#include <iostream>


Character::Character():m_speed(0,0),
m_currentDirection(0, 0),
m_nextDirection(0, 0)
,m_aceleration(13,13)
{
}


Character::~Character()
{
}

void Character::update(sf::Time delta)
{

	//Obtener la posicion del personaje
	sf::Vector2f pixelPosition = getPosition();
	//obtenemos el espacio que se ha movido durante el lapso de tiempo
	sf::Vector2f newVelocity;
	sf::Vector2f pixelTraveled;
	int numx=1,numy=1;


	newVelocity.x = (m_nextDirection.x*getAcceleration().x *delta.asSeconds()) + getSpeed().x;
	newVelocity.y = (m_nextDirection.y*getAcceleration().y *delta.asSeconds()) + getSpeed().y;
	setSpeed(newVelocity);


	/*
	if(newVelocity.x==0)

		 pixelTraveled = sf::Vector2f(m_nextDirection.x*
												newVelocity.x,
												m_nextDirection.y*
												newVelocity.y);
	*/
	
	sf::Vector2f nextPixelPosition = pixelPosition + newVelocity*delta.asSeconds();

	setPosition(nextPixelPosition);

	
}

//me pasan la siguiente direccion y en el update
// me encargare  de moverlo/colisionarlo ...
void Character::setDirection(sf::Vector2i direction)
{
	m_nextDirection = direction;
	std::cout << "-------------------------------" << std::endl;
	std::cout << "Velocity x->" << getSpeed().x << std::endl;
	std::cout << "Velocity y->" << getSpeed().y << std::endl;
}

sf::Vector2i Character::getDirection() const
{
	return sf::Vector2i();
}

void Character::setSpeed(sf::Vector2f speed)
{
	m_speed = speed;
}

sf::Vector2f Character::getSpeed() const
{
	return m_speed;
}

sf::Vector2f Character::getAcceleration() const
{
	return m_aceleration;
}

void  Character::setAcceleration(sf::Vector2f acceleration) 
{
	m_aceleration.x = acceleration.x;
}
