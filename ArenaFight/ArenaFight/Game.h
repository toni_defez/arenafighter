
#ifndef ARENA_FIGHT_GAME_HPP
#define ARENA_FIGHT_GAME_HPP

#include<SFML\Graphics.hpp>
#include "GameState.h"
#include <array>
class Game
{
public:
	Game();
	void run();
	void changeGameState(GameState::State gameState);
	sf::Font& getFont();
	sf::Vector2f getDimensions();

	bool IsFinished();
	void setFinish(bool finish);

	~Game();
private:
	sf::RenderWindow m_window;
	GameState* m_currentState;
	std::array<GameState*, GameState::Count> m_gameStates;
	sf::Font m_font;

	
	bool finished;//variable para controlar el flujo del juego
};

#endif // ARENA_FIGHT_GAME_PP
